# shuffle numbers in range

Hey, here's a small Python cli for printing to the terminal the numbers between a range in order.
By default, it prints a range of [1..10].

## how to use

```
./shuff.py

```
for change default range

```
./shuff.py --min 20 --max 21
```

## requirements

* python3

## linux installation

Please ensure that Python 3 is installed and exists on your system. The python installation may differ depending on your operating system but mostly installation will be like

```
apt-get update && apt-get install -y python3
```

## MacOs installation

you can use Homebrew to install python on mac for more details please read [this](https://docs.python-guide.org/starting/install3/osx/) article.

