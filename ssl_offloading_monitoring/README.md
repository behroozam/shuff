# Monitoring ssl offloading server

* Ssl offloading depends on which cipher suit you are using to encrypt the connection, which is a CPU intensive task, but there are other factors that may have a direct impact on performance, such as disk I/O and memory.

* Network stack is another major component of monitoring.

* It is important to monitor the validity of SSL certificates on the application side as well
and renew within the remaining days.

* Loadbalancer statistics are useful information to monitor.

* Analyzing loadbalancer error logs and shipping them.

### cpu/disk/memmory metrics

cpu load average - cpu utilization per cpu core

disk i/o iostat

memmory utilization by process

### Loadbalancer application metrics(if the ssl offloading is doing by a loadbalancer like nginx and haproxy)
vhosts metrics like active connections, http status response code , error rate

the certification validation (could be done by third party monitoing tools or prometheus blackbox module)

analyse the loadbalancer error logs and set a alert for **ssl** related errros

most important metrics we should monitor:

all connection

open connection per second

closed connection per seocond


### Network stack monitoring and metrics

We are using Bonding to ensure that if one NIC link fails, another link will continue to work. If we don't monitor network stats, we might not have noticed that one of our NICs is failing because the load balancer application is working fine and the traffic is not reaching the limit of one interface.

Dropped packets are another common issue on networks when we do not monitor the connection stability and kernel TCP parameters

for metrics we should monitor:

RTT

inound traffic

outbound traffic

nic stats

network interrupts by cpu

softirq

softnet stat

tcp stats(netstat)

### Challanges

High Avalibility of monitoing in case of failing the infrastructure is crucial.

Improve monitoing is not a one time task, its take a lot of time and effort to collect metrics and set thresholds and alerts.

