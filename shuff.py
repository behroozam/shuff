#!/usr/bin/env python3

import argparse
from random import shuffle

def main():
    parser = argparse.ArgumentParser()

    parser.add_argument('--min', type=int, required=False,
        help="define the minimum number in range", default=1)
    parser.add_argument('--max', type=int, required=False,
        help="define the maximum number in range", default=11)
    args = parser.parse_args()

    min = args.min
    max = args.max

    numbers = list(range(min,max))
    shuffle(numbers)

    print(*numbers)

main()
